<%--
  Created by IntelliJ IDEA.
  User: Joseph
  Date: 11/3/2017
  Time: 11:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/welcome.css">
    <title>Login</title>
</head>
<body>
<form class="login" action="${pageContext.request.contextPath}/login" method="POST">
    <h1 class="login-title">Employee Reimbursement System</h1>
    <p style="color: red">${errorMessage}</p>
    <input type="text" class="login-input" placeholder="Username" autofocus name="user">
    <input type="password" class="login-input" placeholder="Password" name="password">
    <input type="submit" value="Login" class="login-button">
</form>
</body>
</html>