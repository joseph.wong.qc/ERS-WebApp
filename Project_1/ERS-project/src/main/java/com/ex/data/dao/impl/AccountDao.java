package com.ex.data.dao.impl;

import com.ex.data.dao.ERSdao;
import com.ex.domain.Account;
import com.ex.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AccountDao implements ERSdao<Account, Integer>{

    public List<Account> getAll() throws SQLException {
        Connection c = null;
        Account account;
        List<Account> accountList = new ArrayList<Account>();

        try {
            c = ConnectionUtil.newConnection();

            c.setAutoCommit(false);

            String sql = "SELECT USERNAME, PASSWORD, ACCOUNTID, ADMIN FROM ACCOUNT";
            PreparedStatement ps = c.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                account = new Account();
                account.setUsername(rs.getString("USERNAME"));
                account.setPassword(rs.getString("PASSWORD"));
                account.setAccountId(rs.getInt("ACCOUNTID"));
                account.setAdmin(rs.getInt("ADMIN"));
                accountList.add(account);
            }
            c.commit();
            return accountList;

        } catch (SQLException e) {
            e.printStackTrace();
            if(c != null){
                c.rollback();
            }
        } finally {
            if(c != null){
                c.close();
            }
        }

        return accountList;
    }

    public Account findOne(Integer id) throws SQLException {
        Connection c = null;
        Account account = null;

        try {
            c = ConnectionUtil.newConnection();

            c.setAutoCommit(false);

            String sql = "SELECT FIRSTNAME, LASTNAME FROM EMPLOYEE WHERE EMPLOYEEID IN " +
                    "(SELECT EMPLOYEEID FROMM ACCOUNT WHERE ACCOUNTID = ?)";
            PreparedStatement ps = c.prepareStatement(sql);

            ps.setInt(1, id);
            ResultSet rs =  ps.executeQuery();

        } catch (SQLException e){
            e.printStackTrace();
            if(c != null){
                c.rollback();
            }
        } finally {
            if(c != null){
                c.close();
            }
        }


        return account;
    }

    @Override
    public void updateOne(Account o) throws SQLException {

    }
}
