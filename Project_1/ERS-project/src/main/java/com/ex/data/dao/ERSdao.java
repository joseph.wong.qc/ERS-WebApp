package com.ex.data.dao;


import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

public interface ERSdao<T, I extends Serializable>  {
    List<T> getAll() throws SQLException;
    T findOne(I id) throws SQLException;
    void updateOne(T o) throws SQLException;
}
