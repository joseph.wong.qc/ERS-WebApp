package com.ex.data.dao.impl;

import com.ex.data.dao.ERSdao;
import com.ex.domain.Employee;
import com.ex.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDao implements ERSdao<Employee, Integer> {
    @Override
    public List<Employee> getAll() throws SQLException {
        Connection c = null;
        Employee employee;
        List<Employee> employeeList = new ArrayList<Employee>();

        try {
            c = ConnectionUtil.newConnection();

            c.setAutoCommit(false);

            String sql = "SELECT EMPLOYEEID, FIRSTNAME, LASTNAME, ADDRESS, CITY, STATE, ZIPCODE, EMAIL, PHONENUMBER, TITLE FROM EMPLOYEE";
            PreparedStatement ps = c.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                employee = new Employee();
                employee.setEmployeeId(rs.getInt("EMPLOYEEID"));
                employee.setFirstName(rs.getString("FIRSTNAME"));
                employee.setLastName(rs.getString("LASTNAME"));
                employee.setAddress(rs.getString("ADDRESS"));
                employee.setCity(rs.getString("CITY"));
                employee.setState(rs.getString("STATE"));
                employee.setZipCode(rs.getString("ZIPCODE"));
                employee.setEmail(rs.getString("EMAIL"));
                employee.setPhoneNumber(rs.getString("PHONENUMBER"));
                employee.setTitle(rs.getString("TITLE"));
                employeeList.add(employee);
            }
            c.commit();
            return employeeList;

        } catch (SQLException e) {
            e.printStackTrace();
            if (c != null) {
                c.rollback();
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }

        return employeeList;

    }

    @Override
    public Employee findOne(Integer id) throws SQLException {
        Connection c = null;
        Employee employee = null;

        try {
            c = ConnectionUtil.newConnection();

            c.setAutoCommit(false);

            String sql = "SELECT EMPLOYEEID, FIRSTNAME, LASTNAME, ADDRESS, CITY, STATE, ZIPCODE, EMAIL, PHONENUMBER, TITLE FROM EMPLOYEE WHERE EMPLOYEEID = ?";

            PreparedStatement ps = c.prepareStatement(sql);

            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                employee = new Employee();
                employee.setEmployeeId(rs.getInt("EMPLOYEEID"));
                employee.setFirstName(rs.getString("FIRSTNAME"));
                employee.setLastName(rs.getString("LASTNAME"));
                employee.setAddress(rs.getString("ADDRESS"));
                employee.setCity(rs.getString("CITY"));
                employee.setState(rs.getString("STATE"));
                employee.setZipCode(rs.getString("ZIPCODE"));
                employee.setEmail(rs.getString("EMAIL"));
                employee.setPhoneNumber(rs.getString("PHONENUMBER"));
                employee.setTitle(rs.getString("TITLE"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            if (c != null) {
                c.rollback();
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }

        return employee;
    }

    @Override
    public void updateOne(Employee o) throws SQLException {
        Connection c = null;
        Employee employee = o;

        try {
            c = ConnectionUtil.newConnection();

            c.setAutoCommit(false);

            String sql = "UPDATE EMPLOYEE SET FIRSTNAME = ?, LASTNAME = ?, ADDRESS = ?, CITY = ?, STATE = ?, ZIPCODE = ?, EMAIL = ?, PHONENUMBER = ? WHERE EMPLOYEEID = ?";

            PreparedStatement ps = c.prepareStatement(sql);

            ps.setString(1, employee.getFirstName());
            ps.setString(2, employee.getLastName());
            ps.setString(3, employee.getAddress());
            ps.setString(4, employee.getCity());
            ps.setString(5, employee.getState());
            ps.setString(6, employee.getZipCode());
            ps.setString(7, employee.getEmail());
            ps.setString(8, employee.getPhoneNumber());
            ps.setInt(9, employee.getEmployeeId());

            ps.executeUpdate();
            c.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            if (c != null) {
                c.rollback();
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }
}
