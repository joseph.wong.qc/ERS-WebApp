package com.ex.data.dao.impl;

import com.ex.data.dao.ERSdao;
import com.ex.domain.Reimbursement;
import com.ex.util.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ReimbursementDao implements ERSdao<Reimbursement, Integer>{

    @Override
    public List<Reimbursement> getAll() throws SQLException {
        Connection c = null;
        Reimbursement reimbursement;
        List<Reimbursement> reimbursementList = new ArrayList<Reimbursement>();

        try {
            c = ConnectionUtil.newConnection();

            c.setAutoCommit(false);

            String sql = "SELECT REQUESTID, DESCRIPTION, AMOUNT, EMPLOYEEID, HANDLERID, DATESUBMITTED, DATERESOLVED, TYPEID, STATUSID FROM REIMBURSEMENT";
            PreparedStatement ps = c.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                reimbursement = new Reimbursement();
                reimbursement.setRequestId(rs.getInt("REQUESTID"));
                reimbursement.setDescription(rs.getString("DESCRIPTION"));
                reimbursement.setAmount(rs.getFloat("AMOUNT"));
                reimbursement.setEmployeeId(rs.getInt("EMPLOYEEID"));
                reimbursement.setHandlerId(rs.getInt("HANDLERID"));
                reimbursement.setDateSubmitted(rs.getString("DATESUBMITTED"));
                reimbursement.setDateResolved(rs.getString("DATERESOLVED"));
                reimbursement.setTypeId(rs.getInt("TYPEID"));
                reimbursement.setStatusId(rs.getInt("STATUSID"));
                reimbursementList.add(reimbursement);
            }
            c.commit();
            return reimbursementList;

        } catch (SQLException e) {
            e.printStackTrace();
            if(c != null){
                c.rollback();
            }
        } finally {
            if(c != null){
                c.close();
            }
        }

        return reimbursementList;

    }

    @Override
    public Reimbursement findOne(Integer id) throws SQLException {
        Connection c = null;
        Reimbursement reimbursement = null;

        try {
            c = ConnectionUtil.newConnection();

            c.setAutoCommit(false);

            String sql = "SELECT REQUESTID, DESCRIPTION, AMOUNT, EMPLOYEEID, HANDLERID, DATESUBMITTED, DATERESOLVED, TYPEID, STATUSID FROM REIMBURSEMENT WHERE REQUESTID = ?";

            PreparedStatement ps = c.prepareStatement(sql);

            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                reimbursement = new Reimbursement();
                reimbursement.setRequestId(rs.getInt("REQUESTID"));
                reimbursement.setDescription(rs.getString("DESCRIPTION"));
                reimbursement.setAmount(rs.getFloat("AMOUNT"));
                reimbursement.setEmployeeId(rs.getInt("EMPLOYEEID"));
                reimbursement.setHandlerId(rs.getInt("HANDLERID"));
                reimbursement.setDateSubmitted(rs.getString("DATESUBMITTED"));
                reimbursement.setDateResolved(rs.getString("DATERESOLVED"));
                reimbursement.setTypeId(rs.getInt("TYPEID"));
                reimbursement.setStatusId(rs.getInt("STATUSID"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            if(c != null){
                c.rollback();
            }
        } finally {
            if(c != null){
                c.close();
            }
        }

        return reimbursement;
    }

    @Override
    public void updateOne(Reimbursement o) throws SQLException {
        Connection c = null;

        try {
            c = ConnectionUtil.newConnection();

            c.setAutoCommit(false);

            String sql = "UPDATE REIMBURSEMENT SET HANDLERID = ?, STATUSID = ? WHERE REQUESTID = ?";

            PreparedStatement ps = c.prepareStatement(sql);

            ps.setInt(1, o.getHandlerId());
            ps.setInt(2, o.getStatusId());
            ps.setInt(3, o.getRequestId());

            ps.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            if (c != null) {
                c.rollback();
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public void createReimbursement(Reimbursement o) throws SQLException {
        Connection c = null;

        try {
            c = ConnectionUtil.newConnection();

            c.setAutoCommit(false);

            String sql = "INSERT INTO REIMBURSEMENT (EMPLOYEEID, DESCRIPTION, AMOUNT, TYPEID, STATUSID) VALUES (?, ?, ?, ?, ?)";

            PreparedStatement ps = c.prepareStatement(sql);

            ps.setInt(1, o.getEmployeeId());
            ps.setString(2, o.getDescription());
            ps.setFloat(3, o.getAmount());
            ps.setInt(4, o.getTypeId());
            ps.setInt(5, o.getStatusId());

            ps.executeUpdate();

        } catch (SQLException e ) {
            e.printStackTrace();
            if (c != null) {
                c.rollback();
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }


}
