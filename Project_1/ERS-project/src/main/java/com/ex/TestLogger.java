package com.ex;

import org.apache.log4j.Logger;

public class TestLogger {
    final static Logger logger = Logger.getLogger(TestLogger.class);

    public static void main(String[] args) {
//        PropertyConfigurator.configure("WEB-INF/log4j.properties");
        logger.debug("This is DEBUG");
        logger.info("This is INFO");
        logger.warn("This is WARN");
        logger.error("This is ERROR");
        logger.fatal("This is FATAL");
        logger.trace("Entering application.");
            logger.error("Didn't do it.");
        logger.trace("Exiting application.");
    }
}
