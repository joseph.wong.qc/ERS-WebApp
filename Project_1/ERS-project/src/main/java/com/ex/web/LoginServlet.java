package com.ex.web;

import com.ex.domain.Account;
import com.ex.service.LoginService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    private LoginService service =  new LoginService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        String user = request.getParameter("user");
        String password = request.getParameter("password");
        Account account = service.validateUser(user, password);

        if (account != null) {
            HttpSession session = request.getSession();
            session.setAttribute("account", account);
            if(account.getAdmin() == 1){
                request.getRequestDispatcher("/WEB-INF/views/welcome2.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/WEB-INF/views/welcome1.jsp").forward(request, response);
            }
        } else {
            request.setAttribute("errorMessage", "Invalid Credentials!!");
            request.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(request, response);
    }
}
