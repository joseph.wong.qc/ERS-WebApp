package com.ex.web;

import com.ex.domain.Account;
import com.ex.domain.Employee;
import com.ex.service.EmployeeInfoService;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/employee/*")
public class EmployeeServlet extends HttpServlet {

    private EmployeeInfoService service = new EmployeeInfoService();

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = request.getRequestURI().substring(1);
        System.out.println(url);
        String[] urlParts = url.split("/");
        for(String u : urlParts) {
            System.out.println(u);
        }
        request.setAttribute("urlParts", urlParts);
        super.service(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] arr = (String[]) request.getAttribute("urlParts");
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");

        if(arr[2].equals("update")){
            Integer employeeId = account.getAccountId();
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String address = request.getParameter("address");
            String city = request.getParameter("city");
            String state = request.getParameter("state");
            String zipCode = request.getParameter("zipCode");
            String email = request.getParameter("email");
            String phoneNumber = request.getParameter("phoneNumber");
            service.updateEmployeeInfo(new Employee(employeeId, firstName, lastName, address, city, state, zipCode, email, phoneNumber));
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().print("Updated");
            System.out.println("done");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] arr = (String[]) request.getAttribute("urlParts");
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");

        if(arr[2].equals("view")) {
            Employee employee = service.getOne(account.getAccountId());
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(employee);
            response.setContentType("application/json");
            response.getWriter().print(json);
        }

        if(arr[2].equals("view-all")) {
            List<Employee> employeeList = service.getAll();
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(employeeList);
            response.setContentType("application/json");
            response.getWriter().print(json);
        }
    }
}
