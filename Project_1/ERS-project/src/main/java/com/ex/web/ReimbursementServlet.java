package com.ex.web;

import com.ex.domain.Account;
import com.ex.domain.Reimbursement;
import com.ex.service.ReimbursementService;
import com.fasterxml.jackson.databind.ObjectMapper;
import oracle.sql.DATE;
import org.apache.logging.log4j.core.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/reimbursement/*")
public class ReimbursementServlet extends HttpServlet {

//    static Logger log = Logger.getLogger()
    private ReimbursementService service = new ReimbursementService();

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = request.getRequestURI().substring(1);
        System.out.println(url);
        String[] urlParts = url.split("/");
        for (String u : urlParts) {
            System.out.println(u);
        }
        request.setAttribute("urlParts", urlParts);
        super.service(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] arr = (String[]) request.getAttribute("urlParts");
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");

        if (arr[2].equals("submit")) {
            Float amount = Float.parseFloat(request.getParameter("amount"));
            String typeId = request.getParameter("typeId");
            String description = request.getParameter("description");
            service.submitReimbursement(new Reimbursement(description, amount, account.getAccountId() ,Integer.parseInt(typeId)));
            response.getWriter().print("Reimbursement Request Submitted");
        }

        if(arr[2].equals("filter-status")) {
            Integer statusId = Integer.parseInt(request.getParameter("statusId"));
            if(statusId == 4) {
                List<Reimbursement> reimbursementList = service.getAll();
                ObjectMapper objectMapper = new ObjectMapper();
                String json = objectMapper.writeValueAsString(reimbursementList);
                response.setContentType("application/json");
                System.out.println(json);
                response.getWriter().print(json);
            } else {
                List<Reimbursement> reimbursementList = service.getAllById(statusId,"statusId");
                ObjectMapper objectMapper = new ObjectMapper();
                String json = objectMapper.writeValueAsString(reimbursementList);
                response.setContentType("application/json");
                System.out.println(json);
                response.getWriter().print(json);
            }
        }

        if(arr[2].equals("filter-employee")) {
            Integer employeeId = Integer.parseInt(request.getParameter("employeeId"));
            List<Reimbursement> reimbursementList = service.getAllById(employeeId,"employeeId");
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(reimbursementList);
            response.setContentType("application/json");
            response.getWriter().print(json);
        }

        if(arr[2].equals("update")) {
            Integer statusId = Integer.parseInt(request.getParameter("statusId"));
            System.out.println(statusId);
            Integer requestId = Integer.parseInt(request.getParameter("requestId"));
            service.updateReimbursement(new Reimbursement(account.getAccountId(), requestId, statusId));
            List<Reimbursement> reimbursementList = service.getAll();
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(reimbursementList);
            response.setContentType("application/json");
            response.getWriter().print(json);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] arr = (String[]) request.getAttribute("urlParts");
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");

        if (arr[2].equals("view")) {
            List<Reimbursement> reimbursementList = service.getAllById(account.getAccountId(),"employeeId");
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(reimbursementList);
            response.setContentType("application/json");
            response.getWriter().print(json);
        }

        if(arr[2].equals("view-all")) {
            List<Reimbursement> reimbursementList = service.getAll();
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(reimbursementList);
            response.setContentType("application/json");
            response.getWriter().print(json);
        }
    }
}
