package com.ex.domain;

public class Reimbursement {
    private Integer requestId;
    private String description;
    private float amount;
    private Integer employeeId;
    private Integer handlerId;
    private String dateSubmitted;
    private String dateResolved;
    private Integer typeId;
    private Integer statusId;

    public Reimbursement() {
    }
    
    public Reimbursement(Integer employeeId, String description, float amount, Integer typeId){
        this.employeeId = employeeId;
        this.description = description;
        this.amount = amount;
        this.typeId = typeId;
    }

    public Reimbursement(String description, Float amount, Integer accountId, Integer typeId) {
        this.description = description;
        this.amount = amount;
        this.employeeId = accountId;
        this.typeId = typeId;
        this.statusId = 1;
    }

    public Reimbursement(Integer accountId, Integer requestId, Integer statusId) {
        this.handlerId = accountId;
        this.requestId = requestId;
        this.statusId = statusId;
    }

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getHandlerId() {
        return handlerId;
    }

    public void setHandlerId(Integer handlerId) {
        this.handlerId = handlerId;
    }


    public String getDateSubmitted() {
        return dateSubmitted;
    }

    public void setDateSubmitted(String dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    public String getDateResolved() {
        return dateResolved;
    }

    public void setDateResolved(String dateResolved) {
        this.dateResolved = dateResolved;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    @Override
    public String toString() {
        return "Reimbursement{" +
                "requestId=" + requestId +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", employeeId=" + employeeId +
                ", handlerId=" + handlerId +
                ", dateSubmitted='" + dateSubmitted + '\'' +
                ", dateResolved='" + dateResolved + '\'' +
                ", typeId=" + typeId +
                ", statusId=" + statusId +
                '}';
    }
}
