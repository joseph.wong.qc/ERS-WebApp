package com.ex.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {
    private static String url="jdbc:oracle:thin:@oct23java.cv3q82xzrq9c.us-east-2.rds.amazonaws.com:1521:ORCL";
    private static String username="joseph";
    private static String password="password";

    static {
        try{
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());

        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public static Connection newConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }
}
