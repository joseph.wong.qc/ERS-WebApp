package com.ex.service;

import com.ex.data.dao.ERSdao;
import com.ex.data.dao.impl.AccountDao;
import com.ex.domain.Account;

import java.sql.SQLException;
import java.util.List;

public class LoginService {
    private ERSdao<Account, Integer> dao = new AccountDao();

    public Account validateUser(String user, String password) {
        List<Account> accountList = getAll();

        for(Account acc : accountList){
            if(user.equalsIgnoreCase(acc.getUsername()) && password.equals(acc.getPassword())){
                System.out.println("Here");
                return acc;
            }
        }

        return null;
    }

    public List<Account> getAll(){
        try {
            return dao.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
