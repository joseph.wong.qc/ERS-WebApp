package com.ex.service;

import com.ex.data.dao.ERSdao;
import com.ex.data.dao.impl.ReimbursementDao;
import com.ex.domain.Employee;
import com.ex.domain.Reimbursement;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ReimbursementService {

    private ERSdao<Reimbursement, Integer> dao = new ReimbursementDao();

    public List<Reimbursement> getAllById(Integer id, String s) throws JsonProcessingException {
        List<Reimbursement> reimbursementList = new ArrayList<Reimbursement>();
        List<Reimbursement> returnReimbursementList = new ArrayList<Reimbursement>();
        ObjectMapper objectMapper = new ObjectMapper();

        reimbursementList = getAll();
        if(s.equals("employeeId")){
            for(Reimbursement reimbursement : reimbursementList) {
                if (reimbursement.getEmployeeId().equals(id)) {
                    returnReimbursementList.add(reimbursement);
                }
            }
        }
        if(s.equals("statusId")){
            for(Reimbursement reimbursement : reimbursementList) {
                if (reimbursement.getStatusId().equals(id)) {
                    returnReimbursementList.add(reimbursement);
                }
            }
        }

        return returnReimbursementList;
    }

    public void submitReimbursement(Reimbursement reimbursement){
        ReimbursementDao dao = new ReimbursementDao();
        try {
            dao.createReimbursement(reimbursement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateReimbursement(Reimbursement reimbursement){
        try {
            dao.updateOne(reimbursement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Reimbursement> getAll(){
        try {
            return dao.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
