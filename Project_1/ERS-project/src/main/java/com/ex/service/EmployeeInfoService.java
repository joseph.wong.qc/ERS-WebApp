package com.ex.service;

import com.ex.data.dao.ERSdao;
import com.ex.data.dao.impl.EmployeeDao;
import com.ex.domain.Employee;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.sql.SQLException;
import java.util.List;

public class EmployeeInfoService {
    private ERSdao<Employee, Integer> dao = new EmployeeDao();

    public void updateEmployeeInfo(Employee employee) {
        try {
            dao.updateOne(employee);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Employee getOne(Integer id) {
        try {
            return dao.findOne(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Employee> getAll(){
        try {
            return dao.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
