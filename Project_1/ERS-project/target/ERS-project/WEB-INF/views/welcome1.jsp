<%--
  Created by IntelliJ IDEA.
  User: Joseph
  Date: 11/3/2017
  Time: 11:43 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>

<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/http_code.jquery.com_jquery-3.2.1.js"></script>
    <script type="text/javascript" src='${pageContext.request.contextPath}/assets/js/index.js'></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/welcome.css">
    <title>Welcome</title>
</head>
<body>
<div class="navbar">
    <a href="#home" class="click">Home</a>
    <div class="dropdown">
        <button class="dropbtn" onclick="myFunction()">Reimbursement
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content" id="myDropdown">
            <a href="#submit">Submit Reimbursement</a>
            <a href="#view" id="clickViewReimbursement">View Reimbursement</a>
        </div>
    </div>
    <a href="#account" id="account">Account</a>
    <form action="${pageContext.request.contextPath}/logout" method="post">
        <input class="logout" type="submit" value="Logout" />
    </form>
</div>

<div id="content"></div>
</body>
</html>
