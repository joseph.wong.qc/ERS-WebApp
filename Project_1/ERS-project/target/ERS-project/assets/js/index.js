$(document).ready(function () {
    $('#account').click(function (event) {
        $.get('employee/view', function (responseJson) {
            if(responseJson !== null) {
                var myNode = document.getElementById("content");
                myNode.innerHTML = '';
                var table1 = $('#content');
                table1.append('<h2>Your Personal Information</h2>');
                $('<form id="employee-info"></form>').appendTo(table1);
                var table2 = $('#employee-info');
                $('<table id="employee-table"></table>').appendTo(table2);
                var table3 = $('#employee-table');
                table3.append('<tr><td>First Name: </td><td><input name="firstName" value="'+responseJson['firstName'] +'"></td></tr>');
                table3.append('<tr><td>Last Name: </td><td><input name="lastName" value="'+ responseJson['lastName'] +'"></td></tr>');
                table3.append('<tr><td>Address: </td><td><input name="address" value="'+ responseJson['address'] +'"></td></tr>');
                table3.append('<tr><td>City: </td><td><input name="city" value="'+ responseJson['city'] + '"></td></tr>');
                table3.append('<tr><td>State: </td><td><input name="state" value="'+ responseJson['state'] + '"></td></tr>');
                table3.append('<tr><td>Zip Code: </td><td><input name="zipCode" value="'+ responseJson['zipCode'] + '"></td></tr>');
                table3.append('<tr><td>Email: </td><td><input name="email" value="'+ responseJson['email'] + '"></td></tr>');
                table3.append('<tr><td>Phone Number: </td><td><input name="phoneNumber" value="'+ responseJson['phoneNumber'] + '"></td></tr>');
                table3.append('<tr><td>Title: </td><td><input disabled value="'+ responseJson['title'] + '"></td></tr>');
                table2.append('<button id="employee-update">Update</button>')
            }
        })
    })
});

$(document).ready(function () {
    $('#employees').click(function (event) {
        $.get('employee/view-all', function (responseJson) {
            if(responseJson !== null) {
                var table = $('#allEmployee').find('tbody:last-child');
                for(key in responseJson) {
                    table.append('<tr><td>'+responseJson[key]['employeeId']+'</td><td>'+responseJson[key]['firstName']+'</td><td>'+responseJson[key]['lastName']+'</td>><td>'+responseJson[key]['address']+'</td><td>'+responseJson[key]['city']+'</td><td>'+responseJson[key]['state']+'</td><td>'+responseJson[key]['zipCode']+'</td><td>'+responseJson[key]['email']+'</td><td>'+responseJson[key]['phoneNumber']+'</td><td>'+responseJson[key]['title']+'</td></tr>');
                }
            }
        })
    })
});

$(document).on("click", "#employee-update", function (event) {
    event.preventDefault();
    $.post('employee/update', $('#employee-info').serialize(), function (responseJson) {
        alert(responseJson);
        $('#content').load("home.html");
        return false;
    })
});

$(document).ready(function () {
    $('#clickViewReimbursement').click(function (event) {
        $.get('reimbursement/view', function (responseJson) {
            if(responseJson !== null) {
                var table = $('#myReimbursement').find('tbody:last-child');
                for(key in responseJson) {
                    table.append('<tr><td>'+responseJson[key]['description']+'</td><td>'+responseJson[key]['amount']+'</td>><td>'+lookUpType(responseJson[key]['typeId'])+'</td><td>'+responseJson[key]['dateSubmitted']+'</td><td>'+lookUpStatus(responseJson[key]['statusId'])+'</td><td>'+ isNull(responseJson[key]['dateResolved'])+'</td></tr>');
                }
            }
        })
    })
});

$(document).ready(function () {
    $('#clickViewAllReimbursement').click(function (event) {
        $.get('reimbursement/view-all', function (responseJson) {
            if(responseJson !== null) {
                var table = $('#allReimbursement').find('tbody:last-child');
                for(key in responseJson) {
                    table.append('<tr><td>'+responseJson[key]['requestId']+'</td><td>'+responseJson[key]['employeeId']+'</td>><td>'+responseJson[key]['description']+'</td><td>'+responseJson[key]['amount']+'</td><td>'+lookUpType(responseJson[key]['typeId'])+'</td><td>'+responseJson[key]['dateSubmitted']+'</td><td>'+lookUpStatus(responseJson[key]['statusId'])+'</td><td>'+isNull(responseJson[key]['dateResolved'])+'</td><td>'+isNull(responseJson[key]['handlerId'])+'</td><td>'+isResolved(responseJson[key]['statusId'],responseJson[key]['requestId'])+'</td></tr>');
                }
            }
        })
    })
});

$(document).ready(function () {
    $('#content').load("home.html");
});

$(document).ready(function () {
    $('div.navbar a.click').click(function () {
        var page = $(this).attr('href');
        page = page.substr(1);
        $('#content').load(page + '.html');
    })
});

$(document).on("click", ".filter", function() {
    $.post('reimbursement/filter-status', 'statusId='+$(this).val(), function (responseJson) {
        $('#content').load("view-all.html", function () {
            if(responseJson !== null) {
                var table = $('#allReimbursement').find('tbody:last-child');
                for(key in responseJson) {
                    table.append('<tr><td>'+responseJson[key]['requestId']+'</td><td>'+responseJson[key]['employeeId']+'</td>><td>'+responseJson[key]['description']+'</td><td>'+responseJson[key]['amount']+'</td><td>'+lookUpType(responseJson[key]['typeId'])+'</td><td>'+responseJson[key]['dateSubmitted']+'</td><td>'+lookUpStatus(responseJson[key]['statusId'])+'</td><td>'+isNull(responseJson[key]['dateResolved'])+'</td><td>'+isNull(responseJson[key]['handlerId'])+'</td><td>'+isResolved(responseJson[key]['statusId'],responseJson[key]['requestId'])+'</td></tr>');
                }
            }
        })
    })
});


$(document).on("click", ".updateReimbursement", function() {
    $.post('reimbursement/update', 'requestId='+$(this).attr('id')+'&statusId='+$(this).val(), function (responseJson) {
        $('#content').load("view-all.html", function () {
            if(responseJson !== null) {
                var table = $('#allReimbursement').find('tbody:last-child');
                for(key in responseJson) {
                    table.append('<tr><td>'+responseJson[key]['requestId']+'</td><td>'+responseJson[key]['employeeId']+'</td>><td>'+responseJson[key]['description']+'</td><td>'+responseJson[key]['amount']+'</td><td>'+lookUpType(responseJson[key]['typeId'])+'</td><td>'+responseJson[key]['dateSubmitted']+'</td><td>'+lookUpStatus(responseJson[key]['statusId'])+'</td><td>'+isNull(responseJson[key]['dateResolved'])+'</td><td>'+isNull(responseJson[key]['handlerId'])+'</td><td>'+isResolved(responseJson[key]['statusId'],responseJson[key]['requestId'])+'</td></tr>');
                }
            }
        })
    })
});

$(document).on("keypress", "#searchEmployeeId", function(e) {
    if(e.keyCode === 13) {
        $.post('reimbursement/filter-employee', 'employeeId='+$(this).val(), function (responseJson) {
            $('#content').load("view-all.html", function () {
                if(responseJson !== null) {
                    var table = $('#allReimbursement').find('tbody:last-child');
                    for(key in responseJson) {
                        table.append('<tr><td>'+responseJson[key]['requestId']+'</td><td>'+responseJson[key]['employeeId']+'</td>><td>'+responseJson[key]['description']+'</td><td>'+responseJson[key]['amount']+'</td><td>'+lookUpType(responseJson[key]['typeId'])+'</td><td>'+responseJson[key]['dateSubmitted']+'</td><td>'+lookUpStatus(responseJson[key]['statusId'])+'</td><td>'+isNull(responseJson[key]['dateResolved'])+'</td><td>'+isNull(responseJson[key]['handlerId'])+'</td><td>'+isResolved(responseJson[key]['statusId'],responseJson[key]['requestId'])+'</td></tr>');
                    }
                }
            })
        })
    }
});

$(document).on("click", "#submit-btn", function () {
    $.post('reimbursement/submit', $('#submit-form').serialize(), function (responseJson) {
        alert(responseJson);
        $('#content').load("home.html");
    })
});

$(document).ready(function () {
    $('div.navbar div#myDropdown a').click(function () {
        var page = $(this).attr('href');
        page = page.substr(1);
        $('#content').load(page + '.html');
    })
});

function isResolved(number, requestId) {
    if(number === 1) {
        return '<select><option value="0"></option><option class="updateReimbursement" id="'+requestId+'" value="2">Approve</option><option class="updateReimbursement" id="'+requestId+'" value="3">Deny</option></select>';
    } else {
        return '-';
    }

}

function lookUpStatus(number) {
    var table = ["null", "Pending", "Approved", "Denied"];

    return table[number];
}

function lookUpType(number) {
    var table = ["null", "Transportation", "Relocation", "Supplies", "Meal & Entertainment", "Education", "Medical"];

    return table[number];
}

function isNull(i) {
    if (i === null || i === 0)
        return "-";
    else
        return i;
}

/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(e) {
    if (!e.target.matches('.dropbtn')) {
        var myDropdown = document.getElementById("myDropdown");
        if (myDropdown.classList.contains('show')) {
            myDropdown.classList.remove('show');
        }
    }
};
